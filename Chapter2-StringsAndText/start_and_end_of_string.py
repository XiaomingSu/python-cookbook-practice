filename = "spam.txt"
print(filename.startswith("file:"))#false
print(filename.endswith(".txt"))#true

import os
filenames = os.listdir("/Users/xiaoming.su/Desktop")
# print(filenames)

image_names = [name for name in filenames if name.endswith((".png",'.jpg'))]
print(image_names)


from urllib.request import urlopen
def read_data(name):
	if name.startswith(("http:","https:","ftp:")):
		return urlopen(name).read()
	else:
		with open(name) as f:
			return f.read()

data = read_data("http://baidu.com")
print(data)