from collections import Counter

words = [
   'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
   'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
   'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
   'my', 'eyes', "you're", 'under'
]

words_counts = Counter(words)
print words_counts

# most_common
top_three = words_counts.most_common(3)
print top_three

# continue counting
more_words = ['why','are','you','not','looking','in','my','eyes']
for word in more_words:
	words_counts[word] += 1
print words_counts

# update
words_counts.update(more_words)
print words_counts

# combine counts
a = b = words_counts
c = a+b
print c

# substract counts
d = a-b
e = a-b-b
print d # Counter()
print e # empty Counter()