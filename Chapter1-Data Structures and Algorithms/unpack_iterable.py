# Problem: Unpacking a Sequence into Separate Variables
p = (4,5)
x,y = p
print x, y

data = [ 'ACME', 50, 91.1, (2012, 12, 21) ]
name, shares, price, date = data
print name, date

name, shares, price, (year, mon, day) = data
print name, year, mon, day

#mismatch
#x,y,z=p 
#ValueError: need more than 2 values to unpack

#unpack works with what is iterable:
s = 'hello'
a,b,c,d,e = s
print a,b,c,d,e
#throwaway Variables
_, shares, price, _ = data
print shares,_


# Problem:Unpacking Elements from Iterables of Arbitrary Length
record = ('Dave', 'dave@example.com', '773-555-1212', '847-555-1212')
# Python 3 only
# name, email, *phone_numbers = record
# print name, email, phone_numbers