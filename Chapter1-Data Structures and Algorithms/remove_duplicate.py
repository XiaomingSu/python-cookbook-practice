def remove_duplicates(sequence):
	seen = set()
	for item in sequence:
		if item not in seen:
			yield item
			seen.add(item)

items = [1, 5, 2, 1, 9, 1, 5, 10]
unique_items = list(remove_duplicates(items))
# use set() to convert will cause result unordered
print unique_items