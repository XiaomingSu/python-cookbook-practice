prices = {
	'ACME':45.23,
	'AAPL':612.78,
	'IBM': 205.55,
	'HPQ': 37.20,
	'FB': 10.75
}

min_price = min(zip(prices.values(), prices.keys()))
max_price = max(zip(prices.values(), prices.keys()))
print min_price
print max_price

# <zip> returns a iterator in python 3 but a list in python 2

print max(prices) #->IBM
print max(prices.values())  #->612.78

# use key function
print min(prices, key=lambda k: prices[k])
print max(prices, key=lambda k: prices[k])
