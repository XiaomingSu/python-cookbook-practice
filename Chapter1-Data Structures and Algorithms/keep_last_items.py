# Problem: Keeping the Last N Items
from collections import deque
def search(lines, pattern, history=5):
	# automatically push new item and pop old one from the other end
	previous_lines = deque(maxlen=history)
	# deque:O(1) complexity
	# list:O(n)
	for line in lines:
		if pattern in line:
			# generator: previous_lines always before this line
			yield line, previous_lines
		previous_lines.append(line)

with open('keep_last_items.py', 'r') as f:
	for matched_line, previous_lines in search(f,'for', 5):
		print('-'*80)
		for pline  in previous_lines:
			print(pline)
		print("Line Matched::::",matched_line)
		print('-'*80)
