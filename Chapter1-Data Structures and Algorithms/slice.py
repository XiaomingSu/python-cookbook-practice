record = '....................100          .......513.25     ..........'

SHARES = slice(20,32)
PRICE = slice(40,48)

cost = int(record[SHARES]) * float(record[PRICE])
print cost

items = [0,1,2,3,4,5,6]
a = slice(2,4)
print items[2:4]
print items[a]
items[a] = [10,11]
print items
del items[a]
print items

s = 'HelloWorld'*4
a = slice(10, 50, 2)
print a.indices(len(s)) # (10, 40, 2)
print a